const button = document.getElementById("findButton");
button.onclick = function() {
  let typedText = document.getElementById("input").value;
  getAnagrams(typedText);
};

function alphabetize(a) {
  return a
    .toLowerCase()
    .split("")
    .sort()
    .join("")
    .trim();
}

console.log(words);

function getAnagrams(typedText) {
  const anagrams = [];
  let alphaInput = alphabetize(typedText);
  console.log(alphaInput);
  words.forEach(word => {
    let alphaWord = alphabetize(word);
    if (alphaInput === alphaWord) {
      anagrams.push(word);
    }
    return anagrams;
  });
  var x = document.createElement("P");
  var t = document.createTextNode(anagrams);
  x.appendChild(t);
  document.body.appendChild(x);
}
